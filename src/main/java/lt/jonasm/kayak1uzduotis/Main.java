package lt.jonasm.kayak1uzduotis;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException {
        NumberFrequencyCalculator numberFrequencyCalculator = new NumberFrequencyCalculator();
        System.out.println("Enter your integer list (example: 1, 4, 1, 5, 8, 1, 3, 5, 1, 4, 1, 3, 7, 2)");
        System.out.println("Type e if you want to exit.");
        String userInput = numberFrequencyCalculator.readInput();
        if (userInput == null) {
            System.out.println("Program exiting.");
            return;
        }
        List<Integer> userIntList = numberFrequencyCalculator.convertInputToIntList(userInput);
        if (userIntList.isEmpty()) {
            System.out.println("List not entered or entered incorrectly. Please refer to example.\nProgram exiting.");
            return;
        }
        Map<Integer, Integer> frequencyMap = numberFrequencyCalculator.calculateFrequenciesToMap(userIntList);
        String formattedResult = numberFrequencyCalculator.formatGraph(frequencyMap);
        System.out.println(formattedResult);
    }
}
