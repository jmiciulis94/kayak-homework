package lt.jonasm.kayak1uzduotis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class NumberFrequencyCalculator {

    public String readInput() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String userInput = reader.readLine();
        if (userInput.equals("e")) {
            return null;
        }
        return userInput;
    }

    public List<Integer> convertInputToIntList(String input) {
        try {
            return Arrays.stream(input.split("[\\s,]+")).map(Integer::parseInt).collect(Collectors.toList());
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public Map<Integer, Integer> calculateFrequenciesToMap(List<Integer> userIntList) {
        Map<Integer, Integer> frequencyMap = new LinkedHashMap<>();
        Integer min = Collections.min(userIntList);
        Integer max = Collections.max(userIntList);
        for (Integer i = min; i < max + 1; i++) {
            frequencyMap.put(i, Collections.frequency(userIntList, i));
        }
        return frequencyMap;
    }

    public String formatGraph(Map<Integer, Integer> frequencyMap) {
        StringBuilder formattedResult = new StringBuilder();
        Integer minValueInMap = (Collections.min(frequencyMap.keySet()));
        Integer maxFrequencyInMap = (Collections.max(frequencyMap.values()));
        Integer currentMaxFrequency = (Collections.max(frequencyMap.values()));
        for (int i = 0; i < maxFrequencyInMap; i++) {
            for (Map.Entry<Integer, Integer> entry : frequencyMap.entrySet()) {
                int intLength;
                if (entry.getKey().equals(minValueInMap)) {
                    intLength = String.valueOf(entry.getKey()).length();
                } else {
                    intLength = String.valueOf(entry.getKey()).length() + 1;
                }
                String format = "%" + intLength + "s";
                if (entry.getValue().equals(currentMaxFrequency)) {
                    formattedResult.append(String.format(format, "*"));
                    entry.setValue(entry.getValue() - 1);
                } else {
                    formattedResult.append(String.format(format, " "));
                }
            }
            currentMaxFrequency = (Collections.max(frequencyMap.values()));
            formattedResult.append("\n");
        }
        String keys = frequencyMap.keySet().stream().map(String::valueOf).collect(Collectors.joining(" "));
        formattedResult.append(keys).append("\n");

        return formattedResult.toString();
    }
}
