package lt.jonasm.kayak1uzduotis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumberFrequencyCalculatorTest {

    private NumberFrequencyCalculator numberFrequencyCalculator;

    @BeforeEach
    public void setUp() {
        numberFrequencyCalculator = new NumberFrequencyCalculator();
    }

    @Test
    public void convertInputToIntList_correctInputValues_returnsIntList() {
        List<Integer> expectedOutput = new ArrayList<>(Arrays.asList(1, 5, 3, 4, 2));
        assertEquals(expectedOutput, numberFrequencyCalculator.convertInputToIntList("1 5 3 4 2"));
        assertEquals(expectedOutput, numberFrequencyCalculator.convertInputToIntList("1, 5, 3, 4, 2"));
        assertEquals(expectedOutput, numberFrequencyCalculator.convertInputToIntList("1 5, 3, 4 2"));
    }

    @Test
    public void convertInputToIntList_incorrectInputValues_returnsEmptyList() {
        List<Integer> expectedOutput = Collections.emptyList();
        assertEquals(expectedOutput, numberFrequencyCalculator.convertInputToIntList("1 5 3 4 2 c"));
        assertEquals(expectedOutput, numberFrequencyCalculator.convertInputToIntList("a b c d e f"));
        assertEquals(expectedOutput, numberFrequencyCalculator.convertInputToIntList("1: 5 3 .2 3"));
    }

    @Test
    public void convertInputToIntList_emptyInput_returnsEmptyList() {
        List<Integer> expectedOutput = Collections.emptyList();
        assertEquals(expectedOutput, numberFrequencyCalculator.convertInputToIntList(""));
    }

    @Test
    public void calculateFrequenciesToMap_unorderedNumbersList_returnsCorrectMap() {
        Map<Integer, Integer> expectedOutput = new LinkedHashMap<>();
        expectedOutput.put(1, 5);
        expectedOutput.put(2, 1);
        expectedOutput.put(3, 2);
        expectedOutput.put(4, 2);
        expectedOutput.put(5, 2);
        expectedOutput.put(6, 0);
        expectedOutput.put(7, 1);
        expectedOutput.put(8, 1);

        List<Integer> integerList = new ArrayList<>(Arrays.asList(1, 4, 1, 5, 8, 1, 3, 5, 1, 4, 1, 3, 7, 2));

        assertEquals(expectedOutput, numberFrequencyCalculator.calculateFrequenciesToMap(integerList));
    }

    @Test
    public void calculateFrequenciesToMap_positiveAndNegativeNumbersList_returnsCorrectMap() {
        Map<Integer, Integer> expectedOutput = new LinkedHashMap<>();
        expectedOutput.put(-5, 1);
        expectedOutput.put(-4, 0);
        expectedOutput.put(-3, 0);
        expectedOutput.put(-2, 0);
        expectedOutput.put(-1, 1);
        expectedOutput.put(0, 0);
        expectedOutput.put(1, 4);
        expectedOutput.put(2, 1);
        expectedOutput.put(3, 2);
        expectedOutput.put(4, 2);
        expectedOutput.put(5, 1);
        expectedOutput.put(6, 0);
        expectedOutput.put(7, 1);
        expectedOutput.put(8, 1);

        List<Integer> integerList = new ArrayList<>(Arrays.asList(-1, 4, 1, -5, 8, 1, 3, 5, 1, 4, 1, 3, 7, 2));

        assertEquals(expectedOutput, numberFrequencyCalculator.calculateFrequenciesToMap(integerList));
    }

    @Test
    public void calculateFrequenciesToMap_oneNumberList_returnsMap() {
        Map<Integer, Integer> expectedOutput = new LinkedHashMap<>();
        expectedOutput.put(0, 1);

        List<Integer> integerList = new ArrayList<>(Collections.singletonList(0));

        assertEquals(expectedOutput, numberFrequencyCalculator.calculateFrequenciesToMap(integerList));
    }

    @Test
    public void formatGraph_positiveKeysMap_formatsString() {
        String expectedOutput = "*              \n" +
                                "*              \n" +
                                "*   * *        \n" +
                                "* * * * *   * *\n" +
                                "1 2 3 4 5 6 7 8\n";

        Map<Integer, Integer> frequencyMap = new LinkedHashMap<>();
        frequencyMap.put(1, 4);
        frequencyMap.put(2, 1);
        frequencyMap.put(3, 2);
        frequencyMap.put(4, 2);
        frequencyMap.put(5, 1);
        frequencyMap.put(6, 0);
        frequencyMap.put(7, 1);
        frequencyMap.put(8, 1);

        assertEquals(expectedOutput, numberFrequencyCalculator.formatGraph(frequencyMap));
    }

    @Test
    public void formatGraph_positiveAndNegativeKeysMap_formatsString() {
        String expectedOutput = "           *              \n" +
                                "           *              \n" +
                                "           *   * *        \n" +
                                " *     *   * * * * *   * *\n" +
                                "-3 -2 -1 0 1 2 3 4 5 6 7 8\n";

        Map<Integer, Integer> frequencyMap = new LinkedHashMap<>();
        frequencyMap.put(-3, 1);
        frequencyMap.put(-2, 0);
        frequencyMap.put(-1, 1);
        frequencyMap.put(0, 0);
        frequencyMap.put(1, 4);
        frequencyMap.put(2, 1);
        frequencyMap.put(3, 2);
        frequencyMap.put(4, 2);
        frequencyMap.put(5, 1);
        frequencyMap.put(6, 0);
        frequencyMap.put(7, 1);
        frequencyMap.put(8, 1);

        assertEquals(expectedOutput, numberFrequencyCalculator.formatGraph(frequencyMap));
    }

    @Test
    public void formatGraph_oneKeyMap_formatsString() {
        String expectedOutput = " *\n" +
                                " *\n" +
                                " *\n" +
                                " *\n" +
                                "-3\n";

        Map<Integer, Integer> frequencyMap = new LinkedHashMap<>();
        frequencyMap.put(-3, 4);

        assertEquals(expectedOutput, numberFrequencyCalculator.formatGraph(frequencyMap));
    }
}
